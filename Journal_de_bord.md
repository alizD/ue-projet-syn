# UE Projet _Synechococcus_

## Autrices 
Cécile Carpaneto, Alice Lagrange et Alizée Deniel

Master 2 Sciences de la Mer - Sorbonne Université


## Description du projet

Le phytoplancton est un ensemble d'organismes, représentant 50% de la production primaire globale. Il constitue 1% de la biomasse photosynthétique, et se diversifie en de nombreux microorganismes, parmi lesquels se trouvent les cyanobactéries.
Les _Synechococcus_ sont des picocyanobactéries, marines ou dulçaquicoles, responsables de 16% de la production primaire océanique. Elles font partie avec les _Prochlorococcus_ des organismes photosynthétiques les plus abondants sur Terre.

Les _Synechococcus_ marins présentent une importante diversité génétique, et sont répartis sur tous les océans dans des gammes de température diversifiées. Les souches connues sont ainsi rassemblées en groupes génétiques (ou clades), notamment en fonction des milieux dans lesquels ils sont distribués (côtier/océan ouvert, tempéré/tropical, etc.). Les Synechococcus ont dû s'adapter à un large panel de température.

Nous chercherons à mettre en évidence pour le sous-cluster 5.1 les 5 groupes génétiques dominants dans l'environnement, afin d'illustrer la diversité génétique des _Synechococcus_. Nous utiliserons pour cela des séquences de plusieurs souches de _Synechococcus_ appartenant au sous-cluster 5.1 et les comparerons grâce au logiciel MEGA (https://www.megasoftware.net/). Pour continuer, nous déterminerons les préférendas thermiques des clades ainsi définis.




## Jour 1 : Lundi 29/11/2021

1. Présentation de l'UE
2. Présentation et choix des projets
3. Introduction à l'environnement Linux/Cluster
4. Introduction à GitLab et exploration de l'outil

## Jour 2 : Mardi 30/11/2021

 ### PARTIE I Diversité génétique

 #### 1. Préparation du jeu de données utilisés
 Nous avons téléchargé à partir du site Cyanorak (disponible à http://application.sb-roscoff.fr/cyanorak/) les séquences des _Synechococcus_ d'intérêt. Le marqueur utilisé pour les séquences est le gène _petB_.
 Nous avons téléchargé chacune une partie des séquences d'intérêt, avant de les rassembler ensuite pour pouvoir réaliser les arbres phylogénétiques.

<details><summary>Voici les séquences qui seront exploitées pour les analyses :</summary>

```
>A15-24
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaattcaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttttac
tacaagccgactgttgccgaggcctattcctctgttcagtacctgatgacagacgtcagc
ttcggctggttgattcgatcggtgcatcgctggagcgcatcgatgatggtgctgatgctg
attcttcacgtgttccgtgtttacctcaccggcggcttcaagcgtccccgtgagctcacc
tgggtcaccggcgtaaccatggccgtgatcactgtttccttcggtgtcaccggttactcc
cttccttgggaccaggttggttattgggccgtcaagattgtttccggagttccagcagcc
attccagttgtgggtgatttcatggtcgagctgctccgcggtggtgaaagcgttggtcag
tccaccctcactcgcttctacagcctgcacacttttgtgatgccctggctgctcgccgtg
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga

>A15-28
atggcgaactcatcccctgtttacgactggttccaggaacgtctggagatccaggacatt
gctgatgacatcagcaccaaatacgtgccgccccacgtcaacattttctattgcctgggc
ggcatcaccctggtctgcttcctgattcagttcgcgactgggttcgcgatgaccttctat
tacaagccaacggttgccgaggcttattcctctgttcagtacctgatgacggatgtcagc
ttcggctggctgattcgttcggttcatcgctggagcgcctcgatgatggtgctcatgctg
atcctccatgtgttccgtgtgtatctcaccggtggtttcaagcgcccccgtgagctcacc
tgggtcaccggcgtgaccatggctgtgatcacggtttccttcggtgtcaccggctactcc
cttccctgggatcaggtcggttactgggccgtcaagattgtttctggtgttccggctgcc
atccccgttgtgggcgacttcatggttgagcttctccgaggtggcgagagcgtcggacag
tcaaccctcacacgcttctacagcctgcacaccttcgtgatgccctggctcctcgcggtg
ttcatgcttatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>A15-44
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcccccccacgtcaacatcttttattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaaacccaccgttgctgaggcttacaagtcggttgaatacctgatgaccgacgtcagc
ttcggttggctgatccgctctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atcctccatgtgttccgcgtgtacctcaccggcggcttcaagcgtccccgtgagctcact
tgggtcacaggcgtgaccatggcagtcatcaccgtgtcctttggtgtgaccggttactcc
ctcccctgggaccaggttggttactgggccgtgaagatcgtctccggcgtccctgctgcc
atccctgttgttggcgatttcatggtggaactgcttcgcggtggcgaaagcgttggccag
gccacgctcactcgcttctacagccttcacaccttcgtgatgccctggctgctggcggtg
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga

>A15-62
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacatcggttccaagtacgtccccccccacgtcaacattttctattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccactgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
tttggttggctgattcgctcggttcaccgctggagcgcatcaatgatggtgctgatgctc
attcttcacgtgttccgcgtgtacctcaccggcggcttcaagcgtcctcgtgagctcacc
tgggtcaccggcgtgaccatggccgtgatcaccgtctccttcggcgttaccggatactcc
cttccctgggatcaggttggttactgggctgtgaagatcgtttctggcgtccctgctgca
attcccgtggtcggcgacttcatggttgagctgcttcgcggtggcgaaagtgtcggacag
gccactctcacccgcttctacagccttcacacctttgtgatgccgtggctcctggcggtg
ttcatgctcatgcacttcctgatgatcaggaagcagggcatttctggtcccttgtga

>A18-40
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttttac
tacaagccgactgttgcagaggcctattcctctgttcagtacctgatgaccgacgtcagc
ttcggctggttgattcgttcggtgcatcgctggagcgcctcgatgatggtgctgatgctg
attcttcacgtgttccgggtttacctcaccggaggtttcaagcgtccccgtgagctcacc
tgggtcactggcgtgaccatggccgtgatcacagtttctttcggtgtcaccggttactcc
ctgccctgggaccaggttggatattgggccgtcaagattgtttccggcgtcccagcagcc
atcccagttgtcggtgacttcatggtggagctgctccgcggtggcgaaagcgtcggtcag
tccacactcactcgcttctacagcctccacacctttgtgatgccatggctgctcgccgta
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>A18-46.1
atggcgaactcatctcctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttctattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttctac
tacaagccgactgttgcagaggcctattcctctgttcagtatttgatgaccgacgtcagc
ttcggctggttgattcgttcggtgcatcgctggagcgcctcgatgatggtgctgatgctg
attcttcacgtgttccgggtttacctcaccggtggtttcaagcgtccccgtgagctcacc
tgggtcaccggcgtgaccatggccgtgatcacagtttccttcggtgtcaccggttactcc
ctgccctgggaccaagttggttactgggctgtcaagattgtttccggcgtccccgctgcc
atccccgttgtgggtgacttcatggtggagctgctccgcggtggcgaaagtgtcggtcag
tccacactcactcgcttctacagcctccacacctttgtgatgccatggctgctcgccgta
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>BIOS-E4-1
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaattcaggacatt
gctgatgacatcagcagcaagtacgtgcctccacacgtcaacattttttactgcctcggt
ggaatcacacttgtgtgcttcctgattcagttcgcgacaggcttcgcgatgaccttctac
tacaagccgacggtcgctgaggcctacacctccgttcagtacctgatgacggatgtgagt
ttcggctggttgatccgctcggtccatcgctggagtgcctcgatgatggtgctgatgctg
atcctccatgtgttccgtgtctatctgacaggcggtttcaagcgtccccgtgaactcacc
tgggtcaccggcgtgaccatggccgtgatcactgtttcctttggcgtcaccggctattcc
ctcccttgggatcaggttggctactgggctgtgaagatcgtctcgggtgttcctgctgcc
atccctgtggtcggagacttcatggttgaactgcttcgtggcggtgaaagtgttggtcag
tccacgctgacgcgcttttacagccttcatacctttgtgatgccatggcttcttgcggtg
ttcatgctcatgcacttcttgatgatccgaaagcaaggcatttccggtcccttgtga

>BIOS-U3-1
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaattcaggacatt
gctgacgacatcagcagcaaatacgtgcctccacacgtcaacattttttactgtctcggc
ggaatcacccttgtgtgcttcctgattcagttcgcgacaggcttcgcgatgaccttctac
tacaagccgacggtcgctgaggcttacacctctgttcagtacttgatgacggatgtgagt
ttcggctggttgattcgttcggtccatcgctggagcgcctcgatgatggtgctgatgctg
atccttcacgtgttccgcgtctacttgactggtggtttcaagcgtccccgtgaactcacc
tgggtgacaggtgtcaccatggccgtgatcacggtttcttttggcgttacgggctattcc
cttccctgggatcaggttggctactgggctgtgaagatcgtttcaggtgtacctgctgca
attccagtggttggagacttcatggttgaactccttcgcggcggtgagagcgttggtcag
tccacgctgactcgcttttacagccttcacacctttgtgatgccctggcttctggcggtc
ttcatgctgatgcacttcctgatgatccggaagcaaggcatttctggtcccttgtga

>BL107
atggcgaattcttcgcccgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcggcacaaaatacgtccccccccacgtcaatattttttattgtctgggc
ggcatcacgttggtctgctttttgattcagttcgcgacagggttcgcgatgactttctat
tacaagccaacagttgctgaggcctatacctctgttcagtacctgatgacagacgtcagc
ttcggttggctgattcgctcggtgcaccgttggagcgcctcgatgatggtgctcatgctg
attttgcatgtgttccgggtttatctcaccggtggttttaagcgtcctcgtgagctcacc
tgggtgactggcgtcaccatggctgtgatcacggtttcgttcggtgttactggctactcc
ctgccttgggatcaagttggttactgggctgtgaagattgtctccggtgttcccgcagcc
attccagttgttggagacttcatggttgagttactccgtggtggagaaagtgttggtcaa
gcaacgctcactcgcttttacagccttcacactttcgtgatgccttggctcctcgcggtg
ttcatgctcatgcacttcctgatgattcgtaagcaaggtatttccggtcccttgtga

>BOUM118
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttctac
tacaagccgactgttgccgaggcctattcctctgttcagtacttgatgaccgacgtcagc
ttcggctggttgattcgatcggtgcatcgctggagcgcatcgatgatggtgctgatgctg
attcttcacgtgttccgtgtttacctcaccggcggcttcaagcgtccccgtgagctcacc
tgggtcaccggcgtcaccatggccgtgatcactgtttccttcggtgtcaccggttactcc
ctgccctgggaccaagttggttactgggccgtcaagattgtttccggcgtcccagcagcc
atcccagttgtcggtgacttcatggtggagctgctgcgcggtggcgaaagtgtcggtcag
tccacactcactcgcttctacagcctgcacacttttgtgatgccatggctgctcgcggta
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>CC9311
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaattcaggacatc
gctgatgacttcagcaccaagtacgttccgccccacgtcaatattttctattgcctgggc
ggcatcacgcttgtttgcttcctgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgtggctgaggcttacagctcagttcagtacctgatgacagacgtgagc
ttcgggtggctcattcgctcagttcacagatggagcgcctcaatgatggtgctcatgctg
atcctgcatgtcttccgtgtgtacctcacgggtggcttcaagcgtccccgtgagctcact
tgggtgactggtgtgacgatggctgtgatcacggtttccttcggagttaccggatactca
cttccttgggatcaagttggctattgggccgtcaagatcgtttccggagtccccgctgcc
attccagtggttggtgatttcatggttgagcttttgcgtggtggagaaagtgttggccaa
tccacactcactcgcttttacagccttcacacattcgtgatgccttggttgcttgctgtc
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttctggtcctttgtga

>CC9605
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacatcggttccaaatacgtgcccccccacgtcaacattttctattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgactttctat
tacaagcccactgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagt
ttcggttggctgattcgctcggttcaccgctggagcgcatcaatgatggtgctgatgctc
attcttcacgtgttccgcgtgtacctcaccggcggttttaagcgtcctcgtgagctcacc
tgggtcaccggtgtgaccatggccgtgatcaccgtttccttcggcgtgaccggttactcc
ctgccttgggatcaggttggttactgggccgtgaagatcgtttcgggcgtccctgctgcc
attccggtggttggcgacttcatggtggagctgcttcgcggtggcgaaagcgtcggtcag
gcaacgctcacccgcttctacagccttcacacctttgtgatgccttggctgctggcagtg
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga

>CC9902
atggcgaactcttcgcccgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcggcacaaaatacgtccccccccacgtcaatatcttttattgcctgggt
ggcatcacgctggtctgcttcctgatccagttcgcgacagggttcgcgatgactttctat
tacaagccaacagttgctgaggcgtatacctctgttcagtatctgatgactgatgtcagc
ttcggctggctgattcgctcggtgcaccgttggagtgcctcgatgatggtgctcatgctg
attttgcatgtgttccgggtctatctcaccggtggttttaagcgtcctcgtgagctcacc
tgggtgactggtgtcaccatggctgtgatcacggtttcgttcggtgttactggctactcc
cttccctgggatcaggttggttactgggctgtgaagattgtctccggcgttcctgcggcc
attcccgtggttggcgacttcatggttgagttactccgcggtggagaaagcgttggtcaa
gcgactctgactcgcttttacagccttcacaccttcgtgatgccttggctcctcgcggtg
ttcatgctcatgcacttcctgatgatccgtaagcaaggtatttccggtcccttgtga

>KORDI-52
atggcgaactcatcaccggtttacgactggttccaagaacgtctggaaatccaggacatt
gctgacgacatcgggtccaagtacgtgcccccccacgtcaacatcttctattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccacagttgccgaggcctacaagtcggtcgaatatctgatgaccgacgtcagc
tttggttggctgatccgctcggtgcaccgctggagcgcatcaatgatggtgttgatgctc
atccttcacgtgttccgcgtttacctcacaggcggcttcaaacgtccccgtgagctcacc
tgggtcaccggtgtgactatggccgtgatcaccgtgtccttcggtgtgactggttactcc
ctgccctgggaccaggttggttactgggccgtgaagatcgtttccggtgtccctgcggcc
attcctgttgttggagatttcatggtcgaactgcttcgcggtggcgaaagcgttggccag
gccactctcacgcgcttctacagccttcacaccttcgtgatgccctggctgctggcggtg
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>M16.1
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcccccccacgtcaacatcttttattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccactgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
ttcggttggctgatccgctctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atcctgcacgtgttccgtgtgtacctcaccggcggcttcaagcgtccccgtgagctcact
tgggtcaccggtgtgaccatggcagtcatcaccgtgtccttcggtgtgaccggttactcc
cttccctgggaccaggttggttactgggcagtgaagatcgtttccggcgtccctgctgcc
atccctgttgttggcgatttcatggtggaactgcttcgcggtggcgaaagcgttggccag
gccacgcttactcgcttctacagccttcacaccttcgtgatgccctggctgctggcggtg
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>MINOS11
atggcgaattcatcccctgtctacgactggttcaacgagcgtcttgagatccaagacatc
gttgacgacatctcttcgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtctgcttcctaattcagttcgccactggatttgcgatgaccttttat
tacaagcccacggtggtggaggcctacagctccgttcagtacttgatgacggatgtgagc
ttcggttggttgatccgctcggtgcatcgctggagcgccagcatgatggtgctgatgctg
atcctgcacgtcttccgcgtgtacctcactggtggctttaagcgtccccgcgagctcacc
tgggtcaccggcgtgaccatggctgtgatcaccgtgtccttcggtgtgacgggctactcc
ttgccttgggaccaagttggctactgggccgtaaagattgtgtctggtgttcctgctgct
gttcctgtggtcggcgacttcatggttgagctgcttcgcggtggtgagagtgttggccaa
tccaccctcacgcgcttctacagcctccacaccttcgtgttgccctggttgctggcggtg
ttcatgcttgcccacttcctgatgattcgtaagcaaggcatctctggtcccctctga

>MITS9220
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaattcaggacatt
gctgatgacatcagcagcaagtacgtgcctccacacgtcaacattttttactgcctcggt
ggaatcacacttgtgtgtttcctgattcagttcgcgacaggcttcgcgatgaccttctac
tacaaacccacggtcgctgaggcttacacatccgtccagtacctgatgacggatgtgagt
tttggatggttgatccgttcggttcatcgctggagtgcctcgatgatggtgctgatgctg
attctccacgtgttccgggtctatctcactggcggtttcaagcgcccccgtgaactcacc
tgggtcaccggcgtcacgatggctgtgatcactgtctccttcggcgtgacgggctattcc
cttccttgggatcaggtcggctactgggctgtgaagatcgtttcaggtgttcctgctgcc
atcccagtggttggagacttcatggttgaactgctccgaggcggtgaaagtgttggccag
tccacactgactcgcttctacagccttcacacctttgtgatgccatggcttctggcggtg
ttcatgctgatgcacttcctgatgatccggaagcaaggtatttctggtcccttgtga

>MVIR-18-1
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaatccaggacatc
gctgatgatttcagcaccaagtacgttccgccccacgtcaatattttttattgcctgggc
ggcatcacgcttgtttgtttcttgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgttgctgaggcttacagctcagttcagtacctgatgacagatgtgagc
ttcgggtggctcattcgttcagttcacagatggagcgcctcgatgatggtgctcatgctg
attcttcacgtcttccgcgtgtatctcacgggtggctttaaacgtccccgtgagctcact
tgggtcaccggcgtgacgatggctgtgatcaccgtttctttcggtgtcaccggctattca
ctcccttgggatcaagttggctattgggccgtcaagattgtttctggagtgcctgctgcc
attccagttgttggtgacttcatggttgaactgctgcgtggtggtgagagcgttggtcaa
tccacactcactcgcttctacagccttcacacgtttgtgatgccttggttgcttgctgtc
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttctggtcctttgtga

>PROS-9-1
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaatccaggacatc
gctgatgacttcagcaccaagtacgttccgccccacgtcaatattttttattgcctgggc
ggcatcacacttgtttgtttcctgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgttgctgaggcttacagctcagttcagtacctgatgacagatgtgagc
ttcgggtggctcattcgctcagttcacagatggagcgcctcgatgatggtgctcatgctg
attctccacgtcttccgggtgtatctcacaggtggctttaagcgtccccgtgagctcact
tgggtcaccggcgtgacgatggctgtgatcactgtttcttttggtgttaccggctactcg
ctcccttgggatcaagttggttattgggcagtcaaaattgtttctggagtgcctgccgcc
attccagttgttggtgacttcatggttgagctgttgcgtggtggcgaaagcgttggtcaa
tccacactcactcgcttctacagccttcacacgtttgtgatgccttggttgcttgctgtg
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttctggtcctttgtga

>PROS-U-1
atggcgaactcatcaccggtttacgactggttccaggaacgtctggagatccaggacatt
gctgacgacatcggctccaagtacgtgcctccccacgtcaacattttctattgcttgggt
ggcatcacattggtgtgcttcctgatccagttcgcgaccggtttcgcgatgaccttctat
tacaagcccaccgtcgcggaggcttacaagtcggtcgaatacctgatgactgacgtcagc
tttggctggttgattcgctccgtgcaccgctggagcgcctcgatgatggtgctgatgctc
atcctccacgtcttccgcgtctacctcaccggcggtttcaagcgtccccgcgagctcacc
tgggtcaccggcgtcaccatggccgtcatcacagtgtccttcggtgtgaccggttactcc
ctcccctgggatcaggttggttactgggctgtgaagatcgtgtctggtgttcctgcagcc
atcccggttgttggtgacttcatggtcgaactgcttcgtggtggtgaaagtgttggccag
gccacccttacgcgcttctacagcctccacaccttcgtgatgccttggctgctggctgtc
ttcatgctcatgcacttcctgatgatccggaagcagggtatttccggtcccttgtga

>RCC307
atggcgaattcatcccctgtctacgactggttcaacgagcgtcttgagatccaagacatc
gttgacgacatctcttcgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtctgcttcctaattcagttcgccactggatttgcgatgaccttttat
tacaagcccacggtggtggaggcctacagctccgttcagtatttgatgacggatgtgagc
ttcggttggttgattcgctcggtgcatcgctggagcgccagcatgatggtgctgatgctg
atcctgcacgtcttccgcgtgtatctcactggtggctttaagcgtccccgcgagctcacc
tgggtcaccggcgtgaccatggctgtgatcaccgtgtccttcggtgtgactggctactcc
ttgccttgggaccaagtcggctactgggccgtgaagattgtgtctggggttcctgctgca
gttcctgtggtcggcgacttcatggttgagctgcttcgcggtggtgagagcgttggccaa
tcaaccctcacgcgcttctacagcctccacactttcgtgttgccctggttgctggcggtg
ttcatgcttgcccacttcctgatgattcgtaagcaaggcatctctggtcccctctga

>WH8109
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcctccccacgtcaacatcttttattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccaccgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
tttggttggctgatccgctctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atccttcacgtgttccgcgtgtacctcaccggtggcttcaagcgtccccgtgagctcacc
tgggtcaccggcgtgaccatggcagtgatcaccgtgtccttcggtgtgaccggttactcc
ttgccctgggaccaggttggttactgggcagtgaaaattgtttctggagttcctgctgcc
attcctgttgttggcgatttcatggtggaactacttcgtggtggcgaaagcgttgggcag
gccactctcacgcgtttctacagccttcacaccttcgtgatgccctggctgctggcggtg
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga

>WH8103
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttctac
tacaagccgactgttgcagaggcctattcctctgttcagtacctgatgaccgacgtcagc
ttcggctggttgattcgttcggtgcatcgctggagcgcatcgatgatggtgctgatgctg
attcttcacgtgttccgggtctacctcaccggtggtttcaagcgtccccgtgagctcacc
tgggtcaccggcgtgaccatggccgtgatcacagtttccttcggtgtcaccggttactcc
ctgccctgggaccaggttggttattgggccgtcaagattgtttccggcgtcccagcagcc
atcccagttgtgggtgacttcatggtggagctgctccgcggtggcgaaagtgtcggtcag
tccacactcactcgcttctacagcctccacacctttgtgatgccatggctgctcgccgta
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>WH8102
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttattgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttctac
tacaagccgactgttgcagaggcctattcctctgttcagtacctgatgaccgacgtcagc
ttcggctggttgattcgttcggtgcatcgctggagcgcatcgatgatggtgctgatgctg
attcttcacgtgttccgggtctacctcaccggtggtttcaagcgtccccgtgagctcacc
tgggtcaccggcgtgaccatggccgtgatcacagtttccttcggtgtcaccggttactcc
ctgccctgggaccaggttggttattgggccgtcaagattgtttccggcgtcccagcagcc
atcccagttgtgggtgacttcatggtggagctgctccgcggtggcgaaagtgtcggtcag
tccacactcactcgcttctacagcctccacacctttgtgatgccatggctgctcgccgta
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>WH8020
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaattcaggacatc
gctgatgatttcagcaccaagtacgttccgccccacgtcaatattttctattgcctgggc
ggcatcacgcttgtttgcttcctgattcagttcgcgaccgggttcgcgatgaccttctat
tacaagccaactgtggctgaggcttacagctcagttcagtacctgatgaccgacgtgagc
tttggctggctcattcgctccgttcatcgatggagtgcctcaatgatggtgctcatgctg
attctgcacgtcttccgtgtttacctcacaggtggcttcaagcgtccccgtgaactcacc
tgggtaaccggtgtgaccatggctgtgatcacggtttccttcggagtgaccggctactca
ctcccttgggatcaagttggttactgggccgtcaagatcgtttcgggagtccccgctgcc
atcccggtggttggtgacttcatggttgagctgttgcgcggtggagaaagtgttggccag
tccacactcactcgcttctacagccttcacacattcgtgatgccttggttgcttgcagtc
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttctggtcctttgtga

>WH8016
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaatccaggacatc
gctgatgatttcagcaccaagtacgttccgccccacgtcaatattttctattgcctgggc
ggcatcactcttgtttgcttcctgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgttgctgaggcctacagctcagttcagtacctgatgacagatgtgagc
ttcgggtggctcattcgctcagttcacagatggagcgcctcaatgatggtgctcatgctc
attcttcacgtcttccgcgtgtacctcacaggtggctttaagcgtccccgtgagctcacc
tgggtaacgggcgtgaccatggccgtgatcactgtttctttcggtgtaaccggttactca
cttccttgggatcaagtcggttattgggccgtcaagatcgtttctggagtgcccgctgcc
attccagtggttggtgacttcatggttgagctgttgcgtggtggcgagagcgttggtcaa
tccacactcactcgcttctacagcctgcacacgtttgtgatgccctggctgctggctgtc
ttcatgctcatgcacttcttgatgatccgtaagcaaggcatttccggtcctttgtga


>TAK9802
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcccccccacgtcaacatcttctattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgaccgggttcgcgatgaccttctat
tacaagcccaccgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
ttcggttggctgatccgctctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atcctgcacgtgttccgcgtgtacctcaccggcggcttcaagcgtccccgtgagctcact
tgggtcaccggcgtgaccatggcagtcatcaccgtgtccttcggtgtgaccggttactcc
ctcccctgggaccaggttggttattgggccgtgaagatcgtttccggcgtccctgctgcc
atccctgttgttggcgatttcatggtggaactgcttcgcggtggtgaaagcgttggccag
gccacgctcacccgcttctacagccttcacaccttcgtgatgccctggctgctggcggtt
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga


>SYN20
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaatccaggacatc
gctgatgatttcagcaccaagtacgttccgccccacgtcaatattttttattgcctgggc
ggcatcacacttgtttgtttcttgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgttgctgaggcttacagctcagttcagtacctgatgacagatgtgagc
ttcgggtggctcattcgctcagttcacagatggagcgcctcgatgatggtgctcatgctg
attcttcacgttttccgcgtgtatctcacgggtggctttaaacgtccccgtgagctcact
tgggtcaccggcgtgacgatggctgtgatcaccgtttctttcggtgtcaccggctattca
ctcccttgggatcaagttggctattgggccgtcaagattgtttctggagtgcctgctgcc
attccagttgttggtgacttcatggttgaactgttgcgtggtggtgagagcgttggtcaa
tccacactcactcgcttctacagccttcacacgtttgtgatgccttggttgcttgctgtc
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttctggtcctttgtga

>RS9915
atggcgaactcatcccctgtttacgactggttccaggaacgtctggaaatccaggacatt
gctgatgacatcagcacgaaatacgtgccgccccacgtcaacatcttttactgcctgggc
ggcatcaccctggtttgcttcctgattcagttcgcgactgggttcgcgatgaccttttac
tacaagccgactgttgcagaggcctattcctctgttcattacctgatgaccgacgtcagc
ttcggctggttgattcgttcggtgcatcgctggagcgcctcgatgatggtgctgatgctg
attcttcacgtgttccgggtttacctcaccggaggtttcaagcgtccccgtgagctcacc
tgggtcactggcgtgaccatggctgtgatcacagtttctttcggtgtcaccggttactcc
ctgccctgggaccaggttggttactgggcagtcaagattgtttccggcgtcccagcagcc
atcccagttgtcggtgacttcatggtggagctgctccgcggtggcgaaagtgtcggtcag
tccacactcactcgcttctatagcctccacacctttgtgatgccatggctgctcgccgta
tttatgctcatgcacttcctgatgattcggaagcagggcatttctggtcctttgtga


>RS9907
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcccccccacgtcaacatcttttattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccaccgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
ttcggttggctgatccgctctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atcctccacgtgttccgcgtgtacctcaccggcggcttcaagcgtccccgtgagctcact
tgggtcaccggcgtcaccatggcagtcatcaccgtgtcctttggtgtgaccggttactcc
cttccctgggaccaggttggttattgggctgtgaagatcgtctccggcgtccctgcagcc
attcctgttgttggcgatttcatggtggaactgcttcgcggtggcgaaagcgttggccag
gccacgctcactcgcttctacagccttcacactttcgtgatgccctggctgctggccgtg
ttcatgctcatgcacttcctgatgatccggaagcagggcatttctggtcccttgtga

>RS9902
atggcgaactcatcaccggtttacgactggttccaggaacgtctggaaatccaggacatt
gctgacgacattggttccaagtacgtgcccccccacgtcaacatcttttattgcctgggg
ggcatcacgttggtgtgcttcctgatccagttcgcgactgggttcgcgatgaccttctat
tacaagcccaccgttgctgaggcttacaagtcggtcgaatacctgatgaccgacgtcagc
ttcggttggctgatccggtctgtgcaccgctggagcgcatcaatgatggtgttgatgctc
atcctccatgtgttccgcgtgtacctcaccggcggcttcaagcgtccccgcgagctcact
tgggtcaccggcgtgaccatggctgtcatcaccgtgtccttcggtgtgaccggttactcc
ctcccctgggaccaggttggttactgggctgtgaagatcgtttccggcgtccctgctgcc
atccctgttgttggcgatttcatggtggaactgcttcgcggtggcgaaagcgttggtcag
gctacgctcactcgcttctacagccttcacaccttcgtgatgccctggctgctggcggtc
ttcatgctcatgcacttcctgatgattcggaagcagggcatttctggtcccttgtga

>ROS8604
atggcgaactcctcacctgtctacgactggttccaggaacgtcttgaaatccaggacatt
gctgatgatttcagcaccaagtacgttccgccccacgtcaatattttttattgcttgggc
ggcatcacgcttgtttgtttcctgattcagttcgcgaccgggttcgcgatgactttctat
tacaagcccactgttgctgaggcctacagctcagttcagtacctgatgacagatgtgagc
ttcgggtggctcattcgctcagttcacagatggagcgcctcaatgatggtgctcatgctc
attcttcacgtcttccgcgtgtacctcacaggtggctttaagcgtccccgtgagctcacc
tgggtaacgggcgtgaccatggccgtgatcactgtttctttcggtgtcaccggctattca
ctcccttgggatcaagttggttattgggccgtcaagattgtttctggagtgcctgctgcc
attccggttgttggtgacttcatggttgaactgctgcgtggtggtgagagcgttggtcaa
tccacactcactcgcttctacagccttcacacgttcgtgatgccttggttgcttgctgtt
ttcatgctcatgcacttcttgatgatccgtaagcagggcatttccggtcctttgtga

```

</details>

#### 2. Alignement des séquences sur BioEdit

- Import du fichier .txt contenant toutes les séquences du gène _petB_ des souches de _Synechoccus_ sur le logiciel BioEdit.

- Alignement des séquences: **Accessory Application> ClustalW Multiple alignment**. Les paramètres utilisés sont ceux par défaut; **Full Multiple alignment**; **Bootstrap: 1000**. 

- Une fois les séquences alignées, on a sauvegardé le nouveau fichier sous format FASTA: [petB_aligne.fas](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/data/petB_aligne.fas).

#### 3. Création des arbres phylogénétiques sur MEGA

Nous avons importé les séquences alignées sur MEGA afin de pouvoir construire les arbres phylogénétiques.

Dans un premier temps, on a exporté les séquences alignées au format MEGA: **: Data > Export Alignment > MEGA Format**.

Nous avons ensuite construit 2 arbres phylogénétiques:
- "Neighbor-Joining Tree" (NJ) : **Phylogeny > Construct/Test NJ Tree** avec les paramètres suivants:
```
    Test of Phylogeny : Bootstrap method 
    No. Of Boostrap Replications : 1000
    Substitution Type : Nucleotide
    Model/Method : Maximum Composite Likelihood 
    Substitutions to Include : d: Transitions + Transversions 
    Rates among Sites : Uniform Rates 
    Patter among Lineages : Same (Homogeneous)
    Gaps/Missing Data Treatment : Pairwise deletion
    Select Codon Positions : ALL 
    Number of Threads : 3

```
--> on a gardé l'"original tree" 

- "Maximum Likelihood Tree" (ML) étape 1 : Définir modèle à utiliser: **Models > Find Best DNA/Protein Models** avec les paramètres suivants:
```
    Tree to Use : Automatic
    Substition Type : Nucleotide
    Gaps/Missing Data Treatment : Use all Sites 
    Select Codon Positions : ALL 
    Branch Swap Filter : None 
    Number of Threads : 3
```
Voici la sortie des ajustements par maximum de vraisemblance de 24 modèles différents de substitution nucléotidique:
![image](/Images/results_best-fit-substitution-model_screenshot.png)

Les modèles ayant la plus faible valeur de BIC (Bayesian Information Criterion) sont considérés comme décrivant le mieux le modèle de substitution nucéotidique, on choisit donc le modèle **TN93+G+I**.

-  "Maximum Likelihood Tree" (ML) étape 2 : Construire l'arbre avec le modèle indiqué: **Phylogeny > Construct/Test ML Tree** avec les paramètres suivants:
```
    Test of Phylogeny : Bootstrap method 
    No. Of Boostrap Replications : 1000
    Substition Type : Nucleotide
    Model/Method : Tamura-Nei model
    Rates among Sites : Gamma Distributed With Invariant Sites
    Gaps/Missing Data Treatment : Use all Sites 
    Select Codon Positions : ALL 
    Ne pas toucher aux 'Tree inference options' 
    Number of Threads : 3
```
Note: L'arbre NJ est beaucoup plus rapide à exécuter, il permet ainsi une exploration rapide des données. Alors que l'arbre ML est réalisé avec un algorithme beaucoup plus puissant (donc beaucoup plus long à exécuter), lui conférant un plus grand degré de confiance.

Pour chacun des arbres, nous avons associé un code couleur issu de Farrant et al., 2016 (https://doi.org/10.1073/pnas.1524865113, Fig.1.) afin de mettre en évidence les regroupements par clade.

#### 4. Analyse et comparaison des arbres phylogénétiques

L'arbre NJ a permis de déterminer cinq clades différents à partir des séquences étudiées. Il s'agit bien des clades indiqués dans le descriptif des séquences sur Cyanorak (clades I à IV et CRD1). 
L'arbre ML, obtenu plus tard, nous donne le même résultat. Les taux de confiance dans le regroupement sont cependant légèrement différents (ex. : 79 contre 82% à un des embranchements, moins de 5% de différence).
Nous pouvons donc utiliser l'arbre NJ avec confiance pour ces séquences. Il est considéré qu'un taux de regroupement supérieur à 70% est un taux correct ; nous afficherons uniquement les taux de confiance supérieurs à 70%. 

![image](./draft results/arbre_couleurs_inkscape.png)

**Figure 2.** Arbre phylogénétique des souches de _Synechococcus_ marines étudiées selon la méthode Neighbor-Joining (NJ). Les taux de confiance supérieurs à 70% sont affichés sur la figure, exception est faite pour l'affichage des taux de confiance au niveau du noeud précédent l'embranchement de la souche A15-28. Lors de la présence de deux taux de confiance, le premier correspond au taux de confiance de l'arbre NJ, le deuxième à celui de l'arbre ML. Un seul taux de croissance signifie que celui-ci est identique pour les deux arbres.

### PARTIE II Adaptation à la température

#### 5. Calcul des taux de croissance à différentes températures pour chaque souche (Excel)
Dans le fichier excel [Synechococcus Growth Temperature](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/data/Synechococcus_Growth_Temperature_data.xlsx), la cinétique de croissance a été mesurée à différentes températures (de 10°C à 35°C) en 3 réplicats (A, B et C) pour 4 souches différentes de _Synechococcus_, chacune représentative d'un clade:

- MVIR-18-1 (clade I)
- M16.1 (clade II)
- BL107 (clade IV)
- WH8102 (clade III)

Après linéarisation des données, on a calculé les taux de croissance pour chacun des réplicats en déterminant le coefficient directeur de la régression linéaire de la phase exponentielle de croissance des souches de _Synechococcus_.
Voici le [tableau final](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/data/Synechococcus_growth-rate_temperature.csv) obtenu en calculant la moyenne et l'écart-type des 3 réplicats pour chaque souche à chaque température.

#### 6. Représenter les taux de croissance en fonction de la température (R)
On a représenté les préférenda thermiques en traçant les taux de croissance en fonction de la température pour chaque souche représentative d'un clade en utilisant ce [script R](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/Scripts%20R/taux_croissance_synechococcus.R).

![image](/draft%20results/taux_syn_temp.png)
**Figure 3.** [à compléter]

On a pu vérifier que les clades I et IV correspondent à des souches dites froides avec un taux de croissance maximal autour de 22°C alors que les clades II et III sont plutôt des souches dites chaudes avec des taux de croissance maximaux aux alentours de 32°C. Ces préférences thermiques mesurées en laboratoire correspondent à celles des souches dans l'environnement décrites par Farrant et al., 2016. Ils distinguent les souches des milieux froids, tempérés et chauds à partir d'une nouvelle entité appelée ESTU (Ecologically Significant Taxonomic Units), qui correspond à des sous-groupes génétiques au sein des clades, et qui vont vivre dans des milieux très différents.

### REMARQUES ET PERSPECTIVES

**Arbres phylogénétiques :**
- On pourra refaire un arbre à partir des souches d'autres clades, ajoutées dans le nouveau fichier disponible [ici](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/data/syn-petB-supp.txt), de manière à vérifier que les embranchements initiaux sont conservés et déterminer comment les nouveaux clades s'agencent au sein de l'arbre.

**Courbes de croissance :**
- Il n'y a pas de données pour les températures de plus de 25°C pour les souches BL107 et MVIR-18-1, elles n'ont pas réussi à pousser à ces températures ;
- **À noter que la forme de la courbe de croissance typique chez les _Synechococcus_ suit une loi normale à distribution asymétrique (coefficient d'asymétrie/skewness < 0).** À des températures basses, l'activité cellulaire est ralentie : les protéines ne sont alors pas aussi efficaces qu'au préférendum thermique de la souche étudiée. À des températures plus élevées que le préférendum thermique, les protéines sont dénaturées : elles perdent ainsi plus rapidement leur conformation tridimensionnelle avec l'augmentation de la température ;
- Les taux de croissance des souches "froides" sont plus bas que les taux de croissance des souches "chaudes" à leur préférendum thermique respectif. Avec les nouveaux jeux de données en cours de production, il est possible que ce résultat change (mais on le notera ici quand même).

**Préférenda thermiques :**
- Il y a une distinction entre les souches des environnements **froids** et des environnements **chauds** (pas d'intermédiaire visible sur le graphique) ;
- On utilise un représentant de chaque clade pour ce qui est de tirer des conclusions. Ceci a ses limitations (comportement différent des souches au sein d'un même clade ?) ;
- On peut noter le grand écart entre les préférenda (10°C de différence), ce qui nous indique un vaste écart de température de préférence pour les souches de _Synechococcus_, adaptées à leur milieu ;
- La souche WH8102, du clade IIIA, est indiquée comme vivant préférentiellement dans des eaux intermédiaires d'après Farrant et al., 2016. Dans la Figure 3, on peut cependant constater qu'elle a un préférendum thermique considéré comme chaud (la souche M16.1 est indiquée comme appartenant à un clade au préférendum thermique chaud).

**Pour demain :**
- Thèse de Justine Pittera : "La fluidité [de la membrane des cellules] peut aussi être régulée par un second mécanisme, qui consiste en l’insertion d’une ou plusieurs doubles liaisons dans les chaînes carbonées grâce à l’action des enzymes désaturases. [...] Les désaturases agissant sur les acides gras estérifiés sur un glycérolipide sont codées par les gènes _des_."
- Télécharger des séquences de _des_ ABCD de _Synechocystis_ sp. PCC 6803 et les blaster sur les séquences des souches utilisées pour les préférenda thermiques. Ceci permettra de vérifier si les souches considérées ont de tels gènes qui permettraient de réguler leur fluidité membranaire, et donc expliquer à l'échelle du génome leur adaptation aux différentes niches thermiques.

## Jour 3 : Mercredi 01/12/2021

### 1. Visite de la plateforme génomique :
- Les données Cyanorak sont acquises par la culture de souches. Puis l'ADN est extrait et purifié, puis amplifié par PCR. On purifie les amplicons des déchêts de la PCR. Et on séquence avec la méthode de Sanger (différentes tailles de séquences)
- Les données Tara sont acquises dans l'environnement. Puis on séquence avec la technique Illumina (Bridge et Illumina) 
![image](Images/Sch%C3%A9ma_acquisition_donnees.png)


### 2. Adaptation à la température: le cas des désaturases

#### 2.1 Contexte
La fluidité de la membrane des cellules est étroitement liée à sa composition en acides gras. En effet, deux caractéristiques des chaînes acyles peuvent influencer considérablement la fluidité des membranes et ainsi l’activité des protéines qui y sont insérées (notamment les protéines photosynthétiques):
- la longueur des chaînes ;
- l'insertion d'une ou plusieurs doubles laisons dans les chaînes carbonées grâce à l’action des enzymes désaturases (Harwood 1988, Stump et al. 1980).

Ici, on va s'intéresser au deuxième mécanisme: l’introduction du nombre approprié d’insaturations le long de la chaîne acyle diminue la température à laquelle la membrane se rigidifie, et fournit la fluidité nécessaire au bon fonctionnement des membranes. Ces enzymes sont régio- et stéréoselectives, c'est-à-dire que chaque désaturase est capable de catalyser l’insertion d’une double liaison à une position spécifique de la chaîne d’acides gras. Les désaturases agissant sur les acides gras estérifiés sur un glycérolipide sont codées par les gènes des (Chintalapti et al. 2006). 
Le modèle cyanobactérien le mieux caractérisé pour l’étude des désaturases est _Synechocystis_ sp. PCC 6803 (eau douce). Cette souche contient dans son génome quatre gènes des ABCD, qui codent pour les désaturases correspondantes. Ainsi DesA désature le carbone localisé en Δ12, DesB en Δ15, DesC en Δ9 et DesD sur le Δ6 (Figure 20).
![image](/Images/degres-insaturation-acides-gras-cyano.jpg)
**Figure 20.** Degrés d’insaturation des chaînes d’acides gras chez les cyanobactéries. Insertion de doubles
liaisons le long du groupement acyle en position sn-1 par différentes enzymes chez Synechocystis sp. PCC 6803.
Les désaturases DesC, DesA, DesD et DesB introduisent des doubles liaisons sur les carbones des positions Δ9,
Δ12, Δ6 et Δ15 respectivement.

Plusieurs études ont montré que grâce à cet équipement en désaturases, _Synechocystis_ était capable de faire varier le nombre d’insaturations le long des acides gras, en introduisant des doubles liaisons lorsqu’elle est exposée à des températures basses (Wada & Murata 1990, Murata & Wada 1995, Quoc & Dubacq 1997, Chintalapati et al. 2007). [thèse de Juliette Pittera]

#### 2.2 Méthodes utilisées
On va donc essayer de vérifier s'il y présence des gènes _desC_, _desA_, _desD_, _desB_ codant pour ces désaturases chez les cyanobactéries marines:
- Recherche des séquences protéiques des gènes _desABCD_ de _Synechocystis_ sp. PCC 6803 sur la base de données [NCBI](https://www.ncbi.nlm.nih.gov/protein/BAA18169.1?report=fasta) (le lien est un exemple pour la séquence de desA). On regroupe ces séquences dans un fichier .txt: [desABCD-synechocystisPCC6803-proteique.txt](https://gitlab.com/alizD/ue-projet-syn/-/blob/main/data/desABCD-synechocystisPCC6803-proteique.txt)
- tBLASTN de ces séquences une par une sur les gènes des 32 souches de _Synechococcus_ (celles de l'arbre phylogénétique) sur [Cyanorak](http://application.sb-roscoff.fr/project/picocyanobacteria/blast_a_selection/), les résultats sont disponibles [ici](draft results/TBLASTN-desABCD-results.pdf)
![image](Images/screenshot-resultats-TBLASTN.png)
Ceci est une capture d'écran des résultats du tBLASTN. Par exemple pour la première souche BIOS-E4-1, on retrouve le gène _desC4_ suite au tBLASTN avec la séquence protéique de _desC_ de _Synechocystis_ avec 52% d'identité.
- Report de la présence/absence des différents gènes _des_ pour chaque souche dans ce [tableau](https://docs.google.com/spreadsheets/d/1vdIPOXs2JdW8dmIbv5IgdN4C5LxixBFsz5b5RYNr4PY/edit#gid=777575518). 

![image](draft results/screenshot_tableau_desABCD.png)

**Tableau 1.** Présence(gris)/Absence(blanc) des gènes codant pour les désaturases chez différentes souches de _Synechococcus_ sp. appartenant aux principaux clades. [légende à améliorer]


#### 2.3 Résultats et remarques

On distingue bien les clades chauds (II,III) des clades froids (I,IV) par la présence/absence des _des_ spécifiques : 
- _desA2_ est uniquement présent chez les clades chauds ;
- _desA3_ peut être présent aussi bien chez les clades chauds que les clades froids ;
- _desC4_ est uniquement présent chez les clades froids ;
- _desC3_ peut être présent partout ;

- les souches des clades froids présentent des _desC3_, _desC4_ et _desA3_ ;
- les souches des clades chauds présentent des _desC3_ et _desA2_ ;
- les souches du clade chaud présentent en plus un _desA3_ ;
- les souches du clade CRD1 et du sous-cluster 5.3 présentent des _desC4_ et _desA3_ ;
- on notera l'absence totale de _desB_ et _desD_ au sein des souches étudiées : **il s'agit de désaturases présentes uniquement chez les cyanobactéries d'eau douce.**

Plusieurs incohérences sont à noter :
- la souche WH8020, appartenant à un clade froid, présente un _desA3_ (identifié avec Cyanorak) sans présenter de _desC_. Aucun _desC_ n'a été détecté avec le BLAST ni sur Cyanorak. Ceci est peu probable, car les dénaturases fonctionnent en chaîne et la DesA ne peut fonctionner si la DesC n'a pas modifié la chaîne d'acides gras ;
- la souche KORDI-52, appartenant au clade chaud II, ne possède pas le gène _desA2_. Cette souche est la seule de son clade à ne pas la posséder, elle-même étant la seule appartenant au clade II-WPC.

### 3. Données TARA

Les données TARA sont disponibles [ici](data/env_param_SUR.csv) pour l'emplacement des stations et les données abiotiques, et [ici](data/all_desaturases_normalized.csv) pour pour avec l'abondance de chaque désaturase par station.
Le code correspondant à l'exploitation des données est présent [ici](Scripts R/Repartition_syn.R).

Dans cette partie, nous allons représenter les abondances des différentes désaturases sur un fond de carte afin de pouvoir vérifier les résultats obtenus à la section suivante, et voir si les désaturases connaissent bien une répartition spécifique en fonction de la température de l'eau de surface.

#### 3.1 Renommage des colonnes des données :

Les noms des colonnes correspondent à des clusters : il s'agit d'un assemblage de plusieurs souches, et le code numérique permet de distinguer les gènes spécifiques recherchés. Pour une meilleure compréhension des données, nous avons renommé les colonnes comme suit :
```
- CK_00001875 => desA2
- CK_00001343 => desA3
- CK_00000043 => desC3
- CK_00056947 => desC
- CK_00008117 => desC4
- CK_00006606 => desA4
- CK_00008116 => desC6
- CK_00037891 => desC
```
 Toutes les colonnes ne seront pas utilisées. Nous avons fait le choix de ne pas utiliser les colonnes "desC" (pour ne pas répéter l'information), "desA4" et "desC6" (gènes absents des souches étudiées ici). Les gènes _desA4_ et _desC6_ sont spécifiques au sous-cluster 5.2, on ne s'intéresse qu'au cluster 5.1 ici.

#### 3.2 Carte obtenue

Voici un premier résultat de carte :
![image](draft results/Carte1.jpeg)

### Remarques et perspectives pour demain (on a presque finiiii)

1. Faire le lien entre le tableau des désaturases et la carte TARA ;
2. Chercher les différences entre les désaturases (par ex. : quelle différence entre _desC3_ et _desC4_ ?) ce qui permettrait de comprendre leurs présences/absences respectives
3. Pourquoi est-ce que la souche WH8020 fait n'importe quoi --> chercher des informations sur cette souche dans la littérature scientifique
4. Essayer de comprendre l'arbre phylogénétique des _des_ contenu dans l'article de [Pittera et al., 2018](Ressources/Pittera_et_al._2018.pdf) ?
5. Commencer à penser aux figures qui seront utilisées dans le rapport et l'oral


## Jour 4 : Jeudi 02/12/2021

### 1. Discussion des résultats avec Louison

- Données du tableau :
    - gène _desC3_ : possédé par tout le monde --> appelé gène "core", n'est pas spécialement responsable d'une adaptation à la température
    - autres gènes : présents spécifiquement dans certains clades --> gène "accessoire"
    - clades chauds : _desC3_ + **_desA2_** et parfois _desA3_ ; clades froids : _desC3_ + **_desC4_** + _desA3_ ==> _desC4_ et _desA2_ sont des enzymes spécifiques de chacun des thermotypes
    - clades froids : ont plus de désaturases que les clades chauds (on rappelle que le but des désaturases est d'abaisser la température de rigidification de la membrane = adaptation aux milieux plus froids) ;
    - clade II : assemblage typique d'un clade chaud, 2 souches sur 10 ont un gène _desA3_
    - clade III : assemblage typique d'un clade chaud mais présente aussi du _desA3_
 

- Représentation du tableau : mettre les clades uniquement en ligne et les gènes _des_ en colonne pour synthétiser les données du Tableau 1 (mise en évidence des gènes présents/absents pour chaque clade). Voici un brouillon du tableau simplifié:
![image](Images/Schéma-genes-clades.PNG)
 

- Si on compare nos données par rapport à la carte : 
    - I : dans des milieux froids spécifiquement ;
    - II : dans des milieux chauds dans des milieux chauds plus stables que le clade III. Deux souches présentant le gène _desA3_, on peut supposer qu'elles se trouvent dans des zones instables ;
    - III : Mer Méditerranée, dans des milieux chauds avec des événements ponctuels d'abaissement de la température lors de la convection hivernale qui fait remonter des eaux profondes froides ;
    - IV : dans des milieux froids spécifiquement ;
 

- Remarque pour la représentation graphique :
    - ne pas représenter le _desC3_ dans les pie chart puisqu'il est présent partout ?
    - garder le nom des gènes _des_ dans la légende uniquement
    - (trouver un moyen de mettre en évidence la présence/absence de certains gènes selon la température du milieu)

### 2. Présentation et optimisation des figures

#### 2.1 Les tableaux et les figures
On a amélioré les proportions, les légendes et les axes des figures pour faciliter la lecture et la compréhension du message à transmettre.

![image](draft results/arbre_noeuds_légende.png)
**Figure 1.** Arbre phylogénétique de _Synechococcus_ basé sur les séquences du gène _petB_ de souches clonales selon la méthode Neighbor-Joining (NJ). Les cerces au niveau des nœuds indiquent un _bootstrap_ (confiance statistique du regroupement) supérieur à 70%. Les assignations taxonomiques au niveau du clade sont données par les codes de couleur. Les souches en gras sont les souches représentatives des principaux clades (I à IV) qui seront utilisées pour la suite de l’étude.


![image](draft results/taux_syn_temp.png)
**Figure 2.** Températures optimales de croissance de quatre souches de _Synechococcus_ représentatives des principaux clades (I à IV). Pour chaque souche, trois réplicats ont été incubés à différentes températures afin de calculer le taux de croissance moyen à la température correspondante. 

![image](draft results/tableau_desCA_clades.png)
**Tableau 1.** Présence/absence de gènes _des_  des désaturases chez les quatre clades marins dominants de _Synechococcus_. La présence de ces gènes dans plus de 85% des génomes étudiés est indiquée en gris. L'astérisque (*) indique la présence de ces gènes uniquement dans 20% des génomes étudiés. Ce tableau synthétique a été réalisé à partir des résultats de BLAST de séquences protéiques des gènes _des_ issus de la cyanobactérie d'eau douce _Synechocystis PCC 6803_ sur les gènes des 32 souches de _Synechococcus_ étudiées (Figure 1), ainsi que des informations disponibles sur Cyanorak.


#### 2.2 Les cartes
Nous avons amélioré les cartes de répartition des gènes en changeant les couleurs :
![image](draft results/Carte2.jpeg)

Nous avons enlevé les _desC3_ et résumés les des peu abondants dans une catégorie "Autre" : 
![image](draft results/Carte3.jpeg)

Nous avons déterminé la taille du camembert par l'abondance totale :
![image](draft results/Carte4.jpeg)

Nous aimerions mettre la températre de l'eau en fond de carte... En cours...

#### 2.3 À garder en tête
Les données TARA ne sont pas prises aux mêmes saisons : il y a une variation temporelle dans nos données.

### 3. Écriture du rapport et préparation du powerpoint
Le rapport est disponible à [ce lien](https://docs.google.com/document/d/1PS0jbYyiW-zsmoYdpIOzMOsTQqBPoagjd2FHvp6ZGss/edit).
Le powerpoint collaboratif est disponible à [ce lien](https://docs.google.com/presentation/d/1j-DSN5Hi5yFOrD2AlGkMJ3-P3tQVL9EVgLMBtzoZI4M/edit#slide=id.p).

Pour préparer les diapos, on s'est réparties les différentes parties de la manière suivante:
    - Contexte et question scientifique --> Alice
    - Méthodes d'analyse --> Alizée
    - Diversité génétique avec l'arbre phylogénétique --> Alizée
    - Niches thermiques avec les taux de croissance en fonction de la température --> Cécile
    - Transition avec le cas des désaturases chez les cyanobactéries d'eau douce --> Cécile
    - Présence/absence des gènes codant pour les désaturases --> Alice
    - Carte de répartition des gènes _des_ avec les données Tara --> Alizée
    - Discussion/résumé des résultats --> Cécile
    - Limites et perspectives --> Alice

## Jour 5 : Vendredi 03/12/2021

On a finalisé et harmonisé les diapositives pour que les transitions soient fluides. On s'est ensuite entraînées séparément puis ensemble avant la présentation orale à 13h30.
Voici la [version finale de la présentation](Final presentation/CARPANETO-BASTOS-DENIEL-LAGRANGE_SYNECHOCOCCUS.pdf).

**Remarques** à propos de l'oral :
- les cultures des clades I et IV semblent vraiment démontrer des taux de croissance à leur préféredum thermique plus bas que pour les clades II et III
- si on a le même résultat, indiquer la méthode Maximum Likelihood plus robuste plutôt que Neighbor Joining qui est un méthode exploratoire
- la séparation en clades peut-elle être déterminée par d'autres facteurs ?
- comment expliquer que certains clades aient des gènes en particulier et les autres non ? (lateral gene transfert, évolution?)
- il serait intéressant d'ajouter des données de trasncriptomiques pour pouvoir analyser l'expression des gènes codant pour les désaturases, expression différentielle selon la saison?
- limite des données Tara: les échantillons ont été prélevés à différents endroits à différentes saisons --> changement saisonnier des communautés de cyanobactéries marines?

On a finalement discuté et échangé nos impressions sur cette UE Projet entre les différents groupes et tuteurs:
- **les points positifs:** outils informatiques nouveaux et utiles, questions scientifiques intéressantes, visite de la plateforme génomique, bonne balance entre la liberté d'analyse et l'encadrement;
- **les points à améliorer:** évaluer la faisabilité des projets concernant la bioinformatique selon le background du master, ajouter un contexte et un exemple concret à la formation de GitLab.

Merci à tous et à toutes pour cette semaine enrichissante et stimulante!


Bien à vous,

Cécile Carpaneto Bastos, Alizée Deniel et Alice Lagrange.
